resource "random_string" "project_suffix" {
  count = var.projects_count

  length = 30
  upper = false
  special = false
  number = false
}

resource "random_pet" "project_pet_name" {
  count = var.projects_count

  length = 2
  separator = "-"
}

resource "google_project" "projects" {
  count = var.projects_count
  
  name            = substr("${random_pet.project_pet_name[count.index].id}-${random_string.project_suffix[count.index].result}", 0, 30)
  project_id      = substr("${random_pet.project_pet_name[count.index].id}-${random_string.project_suffix[count.index].result}", 0, 30)
  folder_id       = var.folder_id
  billing_account = var.billing_account
}