output "projects" {
  value = google_project.projects.*.project_id
}

output "bindings" {
  value =  sort(distinct([ for b in google_project_iam_member.bindings : "${trimprefix(b.member, "user:" )} ⟷ ${b.project}" ]))
}