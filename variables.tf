// Context
variable "google_state_repository_bucket" {
  description = "Name of the bucket holding the remote terraform state"
}

variable "google_managing_project_id" {
  description = "ID of the project running terraform to create other projects aka. the managing project"
}

variable "google_managing_project_region" {
  description = "Default region of the managing project"
}

// Execution
variable "projects_count" {
  type = number
  description = "Number of projects to create"
}

variable "folder_id" {
  description = "Parent folder ID for any project"
  type = string
}

variable "billing_account" {
  description = "Billing account to associate any project to"
  type = string
}

variable "apis" {
  type = list(string)
  default = []
  description = "List of APIs to enable on each project (e.g `monitoring`, `compute`...)"
}

variable "users" {
  type = list(string)
  default = []
  description = "Users to bind to projects, as in email addresses"
}

variable "roles" {
  type = list(string)
  default = []
  description = "List of roles to affect to users on each project (e.g `editor`, `monitoring.admin`...)"
}