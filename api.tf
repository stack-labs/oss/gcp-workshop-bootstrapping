resource "google_project_service" "apis" {
  count = var.projects_count * length(var.apis)

  project     = google_project.projects[count.index % var.projects_count].project_id
  service     = "${element(var.apis, count.index % length(var.apis))}.googleapis.com"
  disable_on_destroy = false
}
