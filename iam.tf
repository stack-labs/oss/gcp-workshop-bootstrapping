locals {
  affectation_count = min(var.projects_count, length(var.users))
}

resource "google_project_iam_member" "bindings" {
  count = local.affectation_count * length(var.roles)

  project = google_project.projects[count.index % local.affectation_count].project_id
  role     = "roles/${element(var.roles, count.index % length(var.roles))}"
  member     = "user:${var.users[count.index % local.affectation_count]}"
}
